---
title: "Mi Experiencia con Software Libre y Desarrollo de Aplicaciones"
author: Backmota
categories: [Blog]
summary: "Me parece que fue en 2015 cuando conocí a Richard Stallman en una conferencia en CU en la UNAM, y fue la primera ocasión que escuché detalladamente los términos de software libre..."
tags: [Software Libre, Fediverso, iOS, Apple, Desarrollo]
date: 2024-03-23T00:58:38-06:00
draft: false
comments: true
cover: 
    image: "posts/images/fediverse.jpg"
    responsiveImages: true
weight: 1
ShowToc: true
TocOpen: true
---
Me parece que fue en 2015 cuando conocí a Richard Stallman en una conferencia en CU en la UNAM, y fue la primera ocasión que escuché detalladamente los términos de software libre. Entendí que era algo más allá de licencias y una guerra entre GNU/Linux y otros sistemas operativos. Se trata de una filosofía, y la verdad es que es un poco difícil mantenerla al pie de la letra porque estamos rodeados de software privativo. Ahora nosotros somos el producto principal, ya que gratis no es sinónimo de libre. En general, no somos conscientes de cómo utilizamos la tecnología en nuestro beneficio, o mejor dicho, no somos conscientes de cómo la tecnología nos utiliza en su beneficio.

Desde ese momento, comencé a cuestionarme un poco más los servicios que utilizo. En diversas ocasiones incluso llegué a sentirme incómodo con el uso de ciertas plataformas, como fue el caso de Facebook, la cual dejé de utilizar durante varios años. La verdad es que no recuerdo por qué volví a reactivar mi cuenta ahora que lo pienso.

Algunas personas que me conocen desde hace tiempo saben que fui un gran fan de Apple. Lo comento en tiempo pasado porque creo que el corazón de la marca murió con la muerte de Steve Jobs, y el objetivo de esa empresa se enfocó en ser la más valorada monetariamente, pero en términos de innovación, ya no me sorprende.

Y si bien aún no he hecho la migración a un smartphone con algún sistema operativo más libre, como las variaciones de Android como /e/OS o Lineage, que en teoría se encuentran libres de Google, mi teléfono principal hoy sigue siendo un iPhone. Justo ese es mi talón de Aquiles frecuentemente cuando quiero explorar más plataformas libres, como las que conforman la red del Fediverso.

Pero justo hace unos días, después de buscar alguna app que me ayudara a consumir videos de la red de #PeerTube, no logré encontrar ninguna. Fue entonces cuando dije: "Ok, o me muevo a Android, donde sí existen varias apps en F-Droid, o me pongo a hacer una app que consuma el API", y esa fue la opción que decidí tomar.

Hace muchos años aprendí a programar un poco en Objective-C, pero nunca terminé correctamente una app, y en los últimos 5 años no he estado programando. Sin embargo, gracias a ChatGPT y mis conocimientos generales de lógica de programación, conocimiento de API y modelos vista controlador, pude generar una aplicación en tan solo dos días que hace consumo de la instancia fediverse.tv.

Hoy estoy muy feliz de que por fin mandé la app para su aprobación en la App Store, pero mucho más importante, liberé el código con una licencia MIT. Lamentablemente, una licencia GNU no es compatible al ser Swift un lenguaje Open Source pero no libre. Aún así, creo que estoy aportando un poco a la comunidad de software abierto, e incluso ya recibí algunos agradecimientos por Mastodon.

El código de la app lo pueden encontrar en el siguiente enlace:

[Link a GitLab](https://gitlab.com/backmota/fediversetv-ios)


![](../images/fediverse.jpg)
