---
title: "Hola Mundo"
author: Backmota
categories: [Blog]
summary: "Hola a todas, todes y todos soy @backmota, un nerd que lleva navegando en la red aproximadamente desde 1998..."
tags: [Software Libre, Hugo, Tor, Mastodon, Internet]
date: 2023-11-15T15:44:03-06:00
draft: false
comments: true
cover: 
    image: "posts/images/me.jpg"
    responsiveImages: true
weight: 2
ShowToc: true
TocOpen: true
    
---
Hola a todas, todes y todos soy @backmota, un nerd que lleva navegando en la red aproximadamente desde 1998 por lo que me ha tocado diversas épocas desde los modems de 56k con su característico sonido al ingresar a la red, el nacimiento de las redes sociales o la llamada web 2.0 donde todos soñamos con crear nuestra startup unicornio, como tambien  la “democratización del internet, este ultimo punto lo pongo entre comillas porque si bien ahora más personas tienen acceso, las compañías han hecho todo lo posible por monopolizar la atención de las personas en servicios cerrados que incluso alguna vez fueron “mas abiertos” como es el caso del ex twitter ahora X , un espacio que llamo la atención de muchos por su formato de microbloggin donde en su inicio compartíamos el que estábamos haciendo e incluso plantas conectadas a internet que solicitaban agua por medio de tweets, una de las cualidades de esta red social era que podía ser indexada por los motores de búsqueda si tu cuenta no era privada, lo que permitía acceder a información generada en este espacio incluso sin realizar login, pero en los últimos años varios han tomado el modelo negocio que siempre tuvo empresas como Facebook ahora Meta en el cual evitan que la información salga de su territorio y ok sabemos que siempre se trato del dinero, pero el problema es que mucha información se perderá dentro de esos espacios privativos, si bien existen otros espacios aún libres tanto en filosofía como en software como es el caso de **Mastodon** en el cual se siente justo como era twitter hace varios años e incluso mejor, tiene el problema que difícilmente no se va masificar y en ocasiones se extraña poder contactar con usuarios con los que solías platicar, este problema se deriva por diversos factores uno de ellos que al ser tan parecido a X (twitter) la gente lo ve como innecesario, pero existe otro problema por lo menos en México que pone en peligro la **neutralidad de la red** y es que las compañías de telefonía móvil “regalan” internet “ilimitado”en ciertos servicios, principalmente las redes populares y esto vuelve a cerrar aún más las opciones que la gente prefiere para navegar en internet, ya que si llegan a navegar fuera de este cerco el costo de datos es demasiado alto y lo ven como algo que no vale la pena.

Por los motivos que describo anteriormente y otros factores he tomado la iniciativa de retomar un espacio personal, donde no me sienta limitado por ningún termino o condición para poder expresar y comunicar lo que quiero compartir con el mundo, desde que estoy leyendo, que película me gusto o simplemente algo que quiero dejar publico, esto no significa que deje de usar las otras plataformas pero solo serán más una forma de exponer lo que comparta por este medio.

Derivado a los valores que he encontrado en filosofías como lo es el **[Software Libre](https://www.gnu.org/philosophy/free-sw.es.html)** este sitio no cuenta con ningún rastreador de analíticos, ni generá cookies, únicamente crea en tu Local Storage una variable para recordar si tiene el modo oscuro activado o no, en la ejecución de JavaScript únicamente se utiliza **[fuse.js](https://www.fusejs.io)** para tener la funcionalidad de búsqueda, dicha función puede ser desactivada gracias a la extensión de **[LibreJS](https://www.gnu.org/software/librejs/)** o utilizando el navegador **[TOR](https://www.torproject.org)**, pero he puesto vital importancia que el sitio sea totalmente navegable utilizando estás extensiones o navegadores de la red TOR sin ningún problema.

El sitio de momento ha sido construido con **[HUGO](https://gohugo.io)** y se encuentra hospedado en **[GitLab.com](https://gitlab.com/backmota/backmota)** utilizando la función de Pages, si bien es renderizado  por la tecnología de **[CloudFlare](https://www.cloudflare.com/privacypolicy/)**, en un futuro buscaré la posibilidad de migrar el blog a un servidor con **[GNU/Linux-Libre](http://www.fsfla.org/svnwiki/selibre/linux-libre/)** (Por ejemplo **[Trisquel](https://trisquel.info)**) y si es posible en un **[servidor con energía verde](https://es.wikipedia.org/wiki/Green_computing)**.

Gracias por leerme y seguimos en contacto por la red sin que ninguna empresa nos detenga 🤘🏾.


![](../images/me.jpg)