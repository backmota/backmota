---
title: "Mi música favorita"
author: Backmota
categories: [Blog]
summary: "El motivo para hacer este post es tanto compartir un poco sobre mis gustos músicales y tambien funcionarme a base de bitacora de descubrimientos músicales ..."
tags: [Música, Japan, Audio, Plataformas, Music]
date: 2024-02-29T13:34:02-06:00
draft: false
comments: true
cover: 
    image: "posts/images/music.jpg"
    responsiveImages: true
weight: 1
ShowToc: true
TocOpen: true
    
---

El motivo para hacer este post es tanto compartir un poco sobre mis gustos músicales y tambien utilizarla a base de bitacora personal de descubrimientos músicales que voy haciendo y no depender que una plataforma (Youtube, Apple Music, Spotify, etc) sea quien recuerde mis playlist.

En general me considero alguien que disfruta de escuchar discos completos para descubrir las diversas etapadas de los artistas, pero tambien entiendo que muchos artitas actualmente unicamente producen sencillos y no tengo problema con ello, lo tomo como si fuera un disco muy muy corto jeje.

Este será un Post que estaré actualizando, en lugar de crear diversos Post cada que quiera actualizar mi lista.

No haré referencia a una plataforma en particular para que si gustas buscarlos seas libre de elegir el formato de tu preferencia.

Por lo menos en los ultimos 20 años he sido muy fan de la cultura japonesa y he ido descubriendo algunos artistas en diversos momentos, desde compositores de anime como algunos un poco más experimentales, mi favorito en lo particular sería Nujabes que lo descubrí gracias al stream de [@akirareiko](https://es.wikipedia.org/wiki/%C3%93scar_Akira_Yasser_Noriega) el cual se llamaba [#AkiraLateNight](https://www.youtube.com/playlist?list=PL83289A426872BB76).

Algunos de mis artistas Japoneses Favoritos son los siguientes:

* [Nujabes](https://es.wikipedia.org/wiki/Nujabes)
* [Shirō Sagisu](https://es.wikipedia.org/wiki/Shir%C5%8D_Sagisu)
* [Akina Nakamori](https://es.wikipedia.org/wiki/Akina_Nakamori)
* [Tsubomi Daikakumei](https://www.tsubomi-daikakumei.com/) Este grupo idol le tengo un aprecio especial, porque no sabia de su existencia pero en mi primer día en mi viaje por Japón las descubrí en un concierto en la tienda Tokio Tower y termine con un disco firmado por ellas
* [Isao Tomita](https://es.wikipedia.org/wiki/Isao_Tomita)
* [Seiji Yokoyama](https://es.wikipedia.org/wiki/Seiji_Yokoyama)
* [Masayoshi Takanaka](https://es.wikipedia.org/wiki/Masayoshi_Takanaka)
* [Shunsuke Kikuchi](https://es.wikipedia.org/wiki/Shunsuke_Kikuchi)
* [Toshiyuki Omori](https://zh.wikipedia.org/wiki/%E5%A4%A7%E6%A3%AE%E4%BF%8A%E4%B9%8B)
* [Kentaro Haneda](https://es.wikipedia.org/wiki/Kentar%C5%8D_Haneda)

Cuando era niño mi genero músical favorito eran los soundtrack de pélicula y hasta la fecha es algo que disfruto al igual que el soundtrack de video juegos, algunos de los compositores que suelo escuchar son los siguientes:

* [John Williams](https://es.wikipedia.org/wiki/John_Williams_(compositor))
* [Danny Elfman](https://es.wikipedia.org/wiki/Danny_Elfman)
* [Howard Shore](https://es.wikipedia.org/wiki/Howard_Shore)
* [Hans Zimmer](https://es.wikipedia.org/wiki/Hans_Zimmer)
* [Kōji Kondō](https://es.wikipedia.org/wiki/K%C5%8Dji_Kond%C5%8D)
* [Mac Quayle](https://en.wikipedia.org/wiki/Mac_Quayle)

Algo que disfruto bastante es el metal, sobre todo cuando es sinfonico porque justo siento es lo más cercano a soundtracks, si bien en esta lista no va faltar Mägo de Oz y es la banda que más veces he visto en vivo no soy tan fan de sus ultimos trabajos, mi lista indispensable de metal sería:

* [Lacrimosa](https://en.wikipedia.org/wiki/Lacrimosa_(band))
* [Rammstein](https://en.wikipedia.org/wiki/Rammstein)
* [Mägo de oz](https://en.wikipedia.org/wiki/M%C3%A4go_de_Oz)
* [Black Sabbath](https://en.wikipedia.org/wiki/Black_Sabbath)
* [Epica](https://en.wikipedia.org/wiki/Epica_(band))
* [Diablo Swing Orchestra](https://en.wikipedia.org/wiki/Diablo_Swing_Orchestra)
* [Wagakki Band](https://es.wikipedia.org/wiki/Wagakki_Band)
* [Versailles](https://es.wikipedia.org/wiki/Versailles_(banda))

Al final creo me gusta todo lo que suena parecido a los soundtrack y por eso en estas lista no podría falta un poco de Rock Progresivo

* [Pink Floyd ](https://es.wikipedia.org/wiki/Pink_Floyd)
* [The Cure](https://es.wikipedia.org/wiki/The_Cure)
* [Los Jaivas](https://en.wikipedia.org/wiki/Los_Jaivas)
* [Chac Mool](https://es.wikipedia.org/wiki/Chac_Mool_(banda))
* [Real de catorce](https://es.wikipedia.org/wiki/Real_de_Catorce_(banda))
* [El Iconoclasta](https://www.youtube.com/channel/UC8wBVSY8AiVRn7UV8RWh9bw)
* [Mostly Autumn](https://en.wikipedia.org/wiki/Mostly_Autumn)

La música electrónica es algo que igual suelo utilizar como soundtrack de mi vida y en especial la que tiene un sonido un poco más dark, pero al final es un mix de todo


* [Snakeskin](https://es.wikipedia.org/wiki/Snakeskin)
* [Neuvision](https://www.discogs.com/es/artist/4414569-Neuvision)
* [Macross 82-99](https://es.wikipedia.org/wiki/Macross_82-99)
* [Titán](https://es.wikipedia.org/wiki/Tit%C3%A1n_(banda))
* [Silverio](https://www.youtube.com/@epicopico/videos)
* [Jean-MicheL Jarre](https://es.wikipedia.org/wiki/Jean-Michel_Jarre)
* [Selectro-On](https://www.youtube.com/@selectro-onoficial8008)
* [Desconecte](https://www.youtube.com/channel/UCQff6yFvoXJbFKLdek6XW8A)

Otros artistas que suelo escuchar:

* [Accept](https://es.wikipedia.org/wiki/Accept)
* [WarCry](https://es.wikipedia.org/wiki/WarCry)
* [Caifanes](https://es.wikipedia.org/wiki/Caifanes_(banda))
* [Billie Eilish](https://es.wikipedia.org/wiki/Billie_Eilish)
* [Bürdel King](https://es.wikipedia.org/wiki/B%C3%BCrdel_King)
* [Hello SeaHorse](https://es.wikipedia.org/wiki/Hello_Seahorse!)
* [LCD Soundsystem](https://es.wikipedia.org/wiki/LCD_Soundsystem)
* [La Revo](https://es.wikipedia.org/wiki/La_Revoluci%C3%B3n_de_Emiliano_Zapata)
* [Lady Gaga](https://es.wikipedia.org/wiki/Lady_Gaga)
* [Los Pericos](https://es.wikipedia.org/wiki/Los_Pericos)
* [Michael Jackson](https://es.wikipedia.org/wiki/Michael_Jackson)
* [Mitsuto Suzuki](https://es.wikipedia.org/wiki/Masashi_Hamauzu)
* [Nortec](https://es.wikipedia.org/wiki/Nortec_Collective)
* [System of a Down](https://es.wikipedia.org/wiki/System_of_a_Down)
* [Taylor Davis](https://es.wikipedia.org/wiki/Taylor_Davis)
* [Hatsune Miku](https://es.wikipedia.org/wiki/Miku_Hatsune)
* [El Reno Renardo](https://es.wikipedia.org/wiki/El_Reno_Renardo)

![](../images/music.jpg)
